import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import App1 from "./App1";

export default class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="container">
          <nav
            className="navbar"
            role="navigation"
            aria-label="main navigation"
          >
            <div className="navbar-menu">
              <div className="navbar-start">
                <Link to="/" className="navbar-item">
                  App 1
                </Link>
                <Link to="/app-2" className="navbar-item">
                  App 2
                </Link>
              </div>
            </div>
          </nav>

          {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/app-2">
              <About />
            </Route>
            <Route path="/">
              <App1 />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

function About() {
  return (
    <section className="section">
      <div className="container">
        <h1 className="title">App 2 : HOC</h1>
        <h2 className="subtitle">
          A simple container to divide your page into <strong>sections</strong>,
          like the one you're currently reading
        </h2>
      </div>
    </section>
  );
}
