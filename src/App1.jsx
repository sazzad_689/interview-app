import React from "react";

export default class App1 extends React.Component {
  state = {
    todos: [],
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        todos: [
          { id: "1", task: "learn javascript", completed: false },
          { id: "2", task: "learn react", completed: false },
          { id: "3", task: "learn nodejs", completed: false },
        ],
      });
    }, 1500);
  }

  render() {
    return (
      <section className="section">
        <div className="container">
          <h1 className="title">App 1 : Pure Component</h1>
          <ul class="menu-list">
            {this.state.todos.map((item) => (
              <li style={{ marginBottom: "15px" }}>
                <div style={{ display: "inline-block", width: "500px" }}>
                  {item.task}
                </div>
                <button class="button is-info is-small">
                  Mark as complete
                </button>
              </li>
            ))}
          </ul>
        </div>
      </section>
    );
  }
}
